function Mastermind(numbers) {
    this.numbers = numbers;
}

Mastermind.prototype.guess = function (guessed) {
    var plus = 0;
    var minus = 0;

    var plusPositions = [];

    for (var i = 0; i < 4; i++) {
        if (this.numbers[i] === guessed[i]) {
            plus++;
            plusPositions.push(i);
        }
    }

    for (var j = 0; j < 4; j++) {
        if (plusPositions.indexOf(j) !== 0)
            continue;

        var positionInNumbers = this.numbers.indexOf(guessed[j]);
        if (positionInNumbers > -1 && plusPositions.indexOf(positionInNumbers) === -1) {
            minus++;
        }
    }


//    for (var i = 0; i < 4; i++) {
//        if (this.numbers[i] === guessed[i]) {
//            plus++;
//        } else if (this.numbers.indexOf(guessed[i]) > -1) {
//            minus++;
//        }
//    }

    return printI(plus, "+") + printI(minus, "-");
};



Mastermind.prototype.guess2 = function (guessed) {
    var plus = 0;
    var minus = 0;

    var matches = [];

    for (var i = 0; i < 4; i++) {
        if (guessed.indexOf(this.numbers[i]) > -1 && this.numbers.indexOf(guessed[i]) > -1) {
            matches[i] = "-";
        }
    }
    for (var i = 0; i < 4; i++) {
        if (this.numbers[i] === guessed[i]) {
            matches[i] = "+";
        }
    }

    for (var i = 0; i < 4; i++) {
        if (matches[i] === "+") {
            plus++;
        } else if (matches[i] === "-") {
            minus++;
        }
    }


    return printI(plus, "+") + printI(minus, "-");
};

var printI = function (count, sign) {
    var result = "";
    for (var i = 0; i < count; i++) {
        result += sign;
    }
    return result;
};
exports.Mastermind = Mastermind;