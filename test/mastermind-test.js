/*global BowlingGame */
if (typeof require === "function") {
    var buster = require("buster");
    var Mastermind = require("../src/mastermind").Mastermind;
}

var assert = buster.assert;

buster.testCase("MastermindGame", {

    setUp: function() {
        this.game = new Mastermind([1, 0, 2, 4]);
    },

    "returns blank for no right answers": function () {
        var response = this.game.guess([3, 3, 3, 3]);
        assert.equals(response, "");
    },

    "returns minus for one correct number, incorrect position": function () {
        var response = this.game.guess([4, 3, 3, 3]);

        assert.equals(response, "-");

    },

    "returns plus for one correct number, correct position": function () {
        var response = this.game.guess([1, 3, 3, 3]);

        assert.equals(response, "+");
    },

    "returns plus for two correct numbers, correct position, same numbers": function () {
        var response = this.game.guess([1, 3, 2, 3]);
        assert.equals(response, "++");
    },

    "returns plus for two correct numbers, correct position, same numbers": function () {
        var game = new Mastermind([1, 0, 1, 4]);
        var response = game.guess([1, 3, 1, 3]);
        assert.equals(response, "++");
    },

    "returns plus for two correct numbers, correct position, different numbers": function () {
        var response = this.game.guess([1, 3, 2, 3]);
        assert.equals(response, "++");
    },

    "returns plus for two correct numbers, correct position, different numbers, and one minus": function () {
        var response = this.game.guess([1, 4, 2, 3]);
        assert.equals(response, "++-");
    },

    "returns ++++ for all correct": function() {
        var response = this.game.guess([1, 0, 2, 4]);
        assert.equals(response, "++++");
    },

    "returns --- for three correct, wrong positions": function() {
        var response = this.game.guess([0, 1, 4, 3]);
        assert.equals(response, "---");
    },

    "Game [2, 2, 1, 1], guess [0, 2, 2, 1] => ++-": function() {
        var game = new Mastermind([2, 2, 1, 1]);
        var response = game.guess([0, 2, 2, 1]);
        assert.equals(response, "++-");
    },

    "Game [0, 1, 0, 0], guess [1, 1, 2, 2] => +": function() {
        var game = new Mastermind([0, 1, 0, 0]);
        var response = game.guess([1, 1, 2, 2]);
        assert.equals(response, "+");
    }


});